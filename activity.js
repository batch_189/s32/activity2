let http = require ('http');
let port = 8000;

let server = http.createServer((request, response) => {

    if (request.url == '/' && request.method == 'GET'){
        response.writeHead(200, {'content-type': 'text/plain'});
        response.end('Welcome to Booking system');
    };
    if(request.url == '/profile' && request.method == 'GET'){
        response.writeHead(200, {'content-type': 'text/plain'});
        response.end('Welcome to profile');
    };
    if (request.url == '/courses' && request.method == 'GET'){
        response.writeHead(200, {'content-type': 'text/plain'});
        response.end('Welcome to courses');
    };
    if(request.url == '/addCourse' && request.method == 'POST'){
        response.writeHead(200, {'content-type': 'text/plain'});
        response.end('Add course to our resources');
    };

})
server.listen(port);
console.log(`Server in now running at localhost: ${port}`);